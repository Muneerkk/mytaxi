//
//  MyTaxiExtension.swift
//  MyTaxiApp
//
//  Created by Muneer KK on 23/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

import Foundation
import UIKit
extension UIColor {
    class func appYellow()-> UIColor{
            return UIColor(red: 254/255, green: 203/255, blue: 47/255, alpha: 1)
    }
}
extension UITableViewCell {
    func enable(on: Bool) {
        for view in contentView.subviews {
            view.isUserInteractionEnabled = on
            view.alpha = on ? 1 : 0.5
        }
    }
}
