//
//  MyTaxiConstants.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//

import Foundation

import UIKit
let appDelegate = UIApplication.shared.delegate as! AppDelegate

class MyTaxiConstants:NSObject {
    static let baseURL = "https://poi-api.mytaxi.com"
    struct URL {
        static let spotCars          = "/PoiService/poi/v1"
    }
    static let active                = "ACTIVE"
    static let alertButton           = "OK"
    static let alertTitle            = "Failed"
    static let tableViewDefaultCount = 25
    static let pinViewIdentifier     = "AnnotationIdentifier"
    enum ViewController: String {
        case mapView                 = "MapView"
        case carListMapView          = "CarListMapView"
    }
    enum StoryboardName: String {
        case main                    = "Main"
    }
    struct HamburgLocation {
        static let p1Latitude       = 53.694865
        static let p1Longitude      = 9.757589
        static let p2Latitude       = 53.394655
        static let p2Longitude      = 10.099891
    }
    struct APIQuery {
        static let person1Lat       = "p1Lat"
        static let person1Lon       = "p1Lon"
        static let person2Lat       = "p2Lat"
        static let person2Lon       = "p2Lon"
    }
    struct APIKeys {
        static let responseCode     = "ResponseCode"
        static let error            = "error"
    }
    struct AlertMessage {
        static let emptyData        = "Something went wrong. Please try after some time"
    }
    enum TableViewCell: String {
        case carCell                = "CarTableViewCell"
        
    }
}
