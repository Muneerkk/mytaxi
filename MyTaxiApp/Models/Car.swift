//
//  Car.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//

import Foundation
struct Location {
    let p1Lat:Double?
    let p1Lon:Double?
    let p2Lat:Double?
    let p2Lon:Double?
    
}
struct CarListResponse:Codable {
    var carList:[Car]?
    private enum CodingKeys : String, CodingKey {
        case carList = "poiList"
    }
    
}
struct Car:Codable {
    var carID:Int?
    var currentState:String?
    var carType:String?
    var angle:Double?
    var coordinateValue:Coordinate?
    private enum CodingKeys : String, CodingKey {
        case carID      = "id",
        currentState    = "state",
        carType         = "type",
        angle           = "heading",
        coordinateValue = "coordinate"
        
    }
}
struct Coordinate:Codable {
    var latitude:Double?
    var longitude:Double?
}

