//
//  CarListMapViewController.m
//  MyTaxiApp
//
//  Created by Muneer KK on 23/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

#import "CarListMapViewController.h"
#import <MapKit/MapKit.h>
@interface CarListMapViewController()

@end

@implementation CarListMapViewController
@synthesize mapView;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Load all Car location in Map View
    [self loadMapView];
   
    
    // Do any additional setup after loading the view.
}
-(void)loadMapView {
    
    for (CLLocation *location in self.coordinateArray ) {
        // Add an annotation
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = location.coordinate;
        [self.mapView addAnnotation:annotation];
    }
    CLLocation *location  =  self.coordinateArray[0];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];

    

}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
   
    if (annotationView == nil) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        annotationView.canShowCallout = YES;
    } else {
        annotationView.annotation = annotation;
    }
    
    annotationView.image = [UIImage imageNamed:@"carTop.png"];
    
    return annotationView;
}
- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
