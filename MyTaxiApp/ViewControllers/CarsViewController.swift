//
//  ViewController.swift
//  MyTaxiApp
//
//  Created by Calpine on 22/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

import UIKit
import LoadingPlaceholderView
private typealias TableViewMethods = CarsViewController
class CarsViewController: BaseViewController {
    @IBOutlet weak var mapViewButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    fileprivate let carListVm = CarListVM()
    fileprivate var loadingPlaceholderView = LoadingPlaceholderView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup for placeholder view
        setupLoadingPlaceholderView()
        
        // Registering Tableview cell
        tableView.register(UINib(nibName: MyTaxiConstants.TableViewCell.carCell.rawValue, bundle: Bundle.main), forCellReuseIdentifier: MyTaxiConstants.TableViewCell.carCell.rawValue)
        
        // Do any additional setup after loading the view, typically from a nib.
        
        // Loading All cars details
        loadAllCars()
        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func loadAllCars(){
       
        updateUI(enable: false)
        tableView.isHidden = false
        
        carListVm.loadAllCars(location:carListVm.defaultLocation) { [weak self](responseObject ,error) in
            
            // Hide loading placeholder
            guard let `self` = self else {
                return
            }
            if let _ = error {
                self.showAlert(title:MyTaxiConstants.alertTitle, message: (error?.localizedDescription)!)
                self.updateUI(enable: false)
                
            }else {
                DispatchQueue.main.async(execute: { [weak self]() -> Void in
                    guard let `self` = self else {
                        return
                    }
                    
                    let carListResp = try? JSONDecoder().decode(CarListResponse.self, from: responseObject as! Data)
                    if (carListResp != nil) {
                        
                        self.carListVm.carList = (carListResp?.carList)!
                        self.tableView.reloadData()
                        self.updateUI(enable: true)
                    } else {
                       
                       self.showAlert(title:MyTaxiConstants.alertTitle, message: MyTaxiConstants.AlertMessage.emptyData)
                       
                       self.updateUI(enable: false)
                    }
                    
                    
                })
            }
        }
    }
    // Update UI depends on the server response
    fileprivate func updateUI(enable:Bool) {
        mapViewButton.isHidden = !enable
        tableView.isScrollEnabled = enable
        self.tableView.isHidden = !enable
        placeHolderLoadingView(hide: enable)
    }
    fileprivate func setupLoadingPlaceholderView() {
        loadingPlaceholderView.gradientColor = .white
        loadingPlaceholderView.backgroundColor = .white
    }
    fileprivate func placeHolderLoadingView(hide:Bool) {
        if hide {
            loadingPlaceholderView.uncover()
        }else{
            loadingPlaceholderView.cover(tableView)
        }
      
    }


    @IBAction func mapViewButtonPressed(_ sender: Any) {
        let mapVC = UIStoryboard.init(name: MyTaxiConstants.StoryboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: MyTaxiConstants.ViewController.carListMapView.rawValue) as! CarListMapViewController
        let coordinateArray : NSMutableArray = NSMutableArray(array: carListVm.getCoordinateArray(carList:carListVm.carList))
        mapVC.coordinateArray = coordinateArray
        navigationController?.pushViewController(mapVC, animated: true)
        
    }
}
//MARK: - TableView DataSource and Delegate
extension TableViewMethods: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:MyTaxiConstants.TableViewCell.carCell.rawValue, for:indexPath) as! CarTableViewCell
        
        
        guard carListVm.carList.count > 0 else {
            return cell
        }
        let car = carListVm.carList[indexPath.row]
        cell.configureCell(car: car)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Tableview default count is using here for showing Placeholder animation
        
        guard carListVm.carList.count > 0 else {
            return MyTaxiConstants.tableViewDefaultCount
        }
        return carListVm.carList.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let car = carListVm.carList[indexPath.row]
        if car.currentState == MyTaxiConstants.active {
            let mapVC = UIStoryboard.init(name: MyTaxiConstants.StoryboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: MyTaxiConstants.ViewController.mapView.rawValue) as! MapViewController
            mapVC.carListVM.selectedCar = car
            navigationController?.pushViewController(mapVC, animated: true)
        }
        
        
    }
    
    
}


