//
//  MapViewController.swift
//  MyTaxiApp
//
//  Created by Muneer KK on 22/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

import UIKit
import MapKit
private typealias MapViewMethods = MapViewController
class MapViewController: BaseViewController{
@IBOutlet weak var mapView: MKMapView!
    var carListVM = CarListVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let location =  carListVM.selectedCar.coordinateValue else {
            return
        }
        // Loading of Mapview
        loadMapView(location: location)
        // Do any additional setup after loading the view.
    }
    fileprivate func loadMapView(location:Coordinate){
        let center = CLLocationCoordinate2D(latitude: location.latitude!, longitude: location.longitude!)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        mapView.setRegion(region, animated: true)
        mapView.delegate = self
        let annotation = MKPointAnnotation()
        annotation.coordinate = center
        mapView.addAnnotation(annotation)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
extension MapViewMethods: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        
        let annotationIdentifier = MyTaxiConstants.pinViewIdentifier
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }

        // Show Car annotation and image will be rotating based on the angle value
        let pinImage = UIImage(named: "carTop.png")
        annotationView!.image = pinImage
        
        // Image rotation based on the angle value
        annotationView?.transform = CGAffineTransform(rotationAngle: CGFloat(carListVM.selectedCar.angle!))
        
        return annotationView
    }
}
