//
//  CarListMapViewController.h
//  MyTaxiApp
//
//  Created by Muneer KK on 23/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface CarListMapViewController : UIViewController<MKMapViewDelegate>
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property(nonatomic,strong)NSMutableArray *coordinateArray;
@end

NS_ASSUME_NONNULL_END
