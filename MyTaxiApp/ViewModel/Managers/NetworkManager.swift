//
//  NetworkManager.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//

import Foundation
import Alamofire
class NetworkManager: NSObject {
    
    var baseURL = MyTaxiConstants.baseURL
    internal typealias NetWorkApiCompletionBlock = (_ responseObject : Any?, _ errorObject : NSError?) -> ()
    
    /// Enum to handle invalid status codes
    enum StatusCode: Int{
        case successCode = 200
        case noData = 210
    }
    func getRequestPath(url:String, parameters:[String:AnyObject]?, completionBlock:@escaping NetWorkApiCompletionBlock) {
        
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default).responseJSON { [weak self](response) -> Void in
            
            
            guard self != nil else {
                return
            }
            if (response.response?.statusCode) != nil {
                switch (response.result){
                case .success(_):
                    if response.response?.statusCode == StatusCode.successCode.rawValue{
                        
                        completionBlock(response.data, nil)
                        
                    } else {
                        let error = NSError(domain: "",
                                            code: 1,
                                            userInfo:[NSLocalizedDescriptionKey: MyTaxiConstants.AlertMessage.emptyData,NSLocalizedFailureReasonErrorKey: MyTaxiConstants.APIKeys.error])
                        completionBlock(nil, error)
                    }
                    /* to check versonError in result*/
                    
                case .failure(let error):
                    completionBlock(nil, error as NSError? )
                }
                
            }else {
                completionBlock(nil, response.error as NSError? )
                
            }
        }
    }
    
    
    
}
