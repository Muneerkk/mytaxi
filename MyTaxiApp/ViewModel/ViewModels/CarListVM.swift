//
//  MyTaxVM.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//
import Foundation
class CarListVM : BaseVM {
     var apiHandler: CarListAPIHandler    = CarListAPIHandler()
     var dataHandler: CarListDataHandler   = CarListDataHandler()
     let defaultLocation = Location.init(p1Lat: MyTaxiConstants.HamburgLocation.p1Latitude, p1Lon: MyTaxiConstants.HamburgLocation.p1Longitude, p2Lat: MyTaxiConstants.HamburgLocation.p2Latitude, p2Lon: MyTaxiConstants.HamburgLocation.p2Longitude)
    var carList = [Car]()
    var selectedCar = Car()

    // Load car list from server
    func loadAllCars(location:Location,_ onCompletion:@escaping VMDataCompletionBlock){
        apiHandler.spotCars(location) {
            (responseObject, errorObject) -> () in
            
                onCompletion(responseObject as AnyObject,errorObject)
            
        }
    }
    // Load coordinate Array
    func getCoordinateArray(carList:[Car])->[AnyObject] {
        return dataHandler.createCordinateArray(carList: carList)
    }
}
