//
//  BaseVM.swift
//  MyTaxiApp
//
//  Created by Calpine on 22/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

import Foundation
import UIKit

class BaseVM: NSObject {
    
    internal typealias VMCompletionBlock = (_ errorObject : Error?) -> ()
    internal typealias VMDataCompletionBlock = (_ responseObject : AnyObject?, _ errorObject : NSError?) -> ()
}
