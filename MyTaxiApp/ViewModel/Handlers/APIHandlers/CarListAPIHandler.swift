//
//  CarListAPIHandler.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//

import Foundation
class CarListAPIHandler: BaseAPIHandler {
    
    // Create URL compoenents
    func spotCars(_ location:Location, onCompletion:@escaping ApiCompletionBlock){
        var components = URLComponents(string: MyTaxiConstants.baseURL + MyTaxiConstants.URL.spotCars)
        let p1Lat = URLQueryItem(name: MyTaxiConstants.APIQuery.person1Lat, value:"\(location.p1Lat ?? 0.0)")
        let p1Lon = URLQueryItem(name: MyTaxiConstants.APIQuery.person1Lon, value: "\(location.p1Lon ?? 0.0)")
        let p2Lat = URLQueryItem(name: MyTaxiConstants.APIQuery.person2Lat, value: "\(location.p2Lat ?? 0.0)")
        let p2Lon = URLQueryItem(name: MyTaxiConstants.APIQuery.person2Lon, value: "\(location.p2Lon ?? 0.0)")
        components!.queryItems = [p1Lat,p1Lon,p2Lat,p2Lon]
        let urlpath = components?.string
        networkManager.getRequestPath(url: urlpath!, parameters: nil) { (responseObject, errorObject) -> () in
            onCompletion(responseObject as AnyObject, errorObject)
        }
    }
}
