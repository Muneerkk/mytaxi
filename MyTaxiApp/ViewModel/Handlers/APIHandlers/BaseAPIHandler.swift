//
//  BaseAPIHandler.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//

import Foundation
import UIKit

class BaseAPIHandler: NSObject {
    internal typealias ApiCompletionBlock = (_ responseObject : AnyObject?, _ errorObject : NSError?) -> ()
    internal var networkManager : NetworkManager = NetworkManager()
    
    
    //MARK: - initializers
    override init(){
    }
    
}
