//
//  MyTaxiDataHandler.swift
//  MyTaxi
//
//  Created by Muneer KK on 20/11/18.
//  Copyright © 2018 Muneer KK. All rights reserved.
//

import Foundation
class CarListDataHandler: BaseDataHandler {
    
    // Create Loction Model
    func createCordinateArray(carList:[Car])->[AnyObject] {
        var coordinateList = [AnyObject]()
        carList.forEach { (car) in
            let location = CLLocation(latitude: (car.coordinateValue?.latitude)!, longitude: (car.coordinateValue?.longitude)!)
          coordinateList.append(location)
        }
        return coordinateList       
    
    }
}
