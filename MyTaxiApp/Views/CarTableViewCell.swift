//
//  CarCellTableViewCell.swift
//  MyTaxiApp
//
//  Created by Calpine on 22/11/18.
//  Copyright © 2018 Muneer. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(car:Car){
        typeLabel.layer.cornerRadius = 5.0
        typeLabel.layer.borderColor = UIColor.appYellow().cgColor
        typeLabel.layer.borderWidth = 1.0
        
        self.stateLabel.text = car.currentState
        self.typeLabel.text  = car.carType
        
        // Updating cell UI as per the cae state . If car is inactive cell will be disabled
        if car.currentState == MyTaxiConstants.active  {
            self.enable(on: true)
        } else {
            self.enable(on: false)
        }
    }
    
}

